/*
 * Implement bukkit plugin interface
 */

package xeth.oldregen

import java.io.File
import java.util.UUID
import org.bukkit.Bukkit
import org.bukkit.entity.Entity
import org.bukkit.entity.EntityType
import org.bukkit.entity.Player
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.event.EventHandler
import org.bukkit.event.EventPriority
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityDamageEvent
import org.bukkit.event.player.PlayerLoginEvent
import org.bukkit.event.player.PlayerItemConsumeEvent
import org.bukkit.scheduler.BukkitRunnable
import org.bukkit.configuration.file.YamlConfiguration

/**
 * Max player health in mineman
 */
const val MAX_HEALTH: Double = 20.0

public class OldRegenListener(val plugin: OldRegenPlugin): Listener {
    // add some delay between regen task and event
    @EventHandler(priority = EventPriority.NORMAL)
    public fun onDamageEvent(event: EntityDamageEvent) {
        if ( event.getEntityType().equals(EntityType.PLAYER) ) {
            val p = event.getEntity() as Player
            if ( p.getHealth() < MAX_HEALTH && p.getFoodLevel() >= plugin.minHungerToRegen ) {
                if ( !plugin.regenPlayerIds.contains(p.getUniqueId()) ) {
                    plugin.addPlayerRegen(p, plugin.regenDelay)
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public fun onEatingEvent(event: PlayerItemConsumeEvent) {
        val p = event.getPlayer()
        if ( p.getHealth() < MAX_HEALTH && p.getFoodLevel() >= plugin.minHungerToRegen ) {
            if ( !plugin.regenPlayerIds.contains(p.getUniqueId()) ) {
                plugin.addPlayerRegen(p, plugin.regenDelay)
            }
        }
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public fun onLoginEvent(event: PlayerLoginEvent) {
        val p = event.getPlayer()
        if ( p.getHealth() < MAX_HEALTH && p.getFoodLevel() >= plugin.minHungerToRegen ) {
            if ( !plugin.regenPlayerIds.contains(p.getUniqueId())) {
                plugin.addPlayerRegen(p, plugin.regenDelay)
            }
        }
    }
}

public class OldRegenPlugin : JavaPlugin() {
    // only support up to `maxPlayers`, defined by config
    internal var playersRegenerating: Array<Player?> = Array(1024, {_ -> null})
    internal var regenTick: IntArray = IntArray(1024)
    internal var regenPlayerIds: HashSet<UUID> = hashSetOf()
    internal var numRegen: Int = 0 // number of players regen, e.g. array length

    internal var tempBufferPlayers: Array<Player?> = Array(1024, {_ -> null})
    internal var tempBufferTick: IntArray = IntArray(1024)

    internal var maxPlayers: Int = 1024
    internal var regenPeriod: Int = 40
    internal var minHungerToRegen: Int = 18
    internal var checkPlayerRegenPeriod: Int = 10
    internal var regenDelay: Int = 20
    internal var regenRate: Double = 1.0

    internal var checkPlayerRegenTick: Int = 10

    private var task: HealthRegenTask? = null

    /**
     * Regen task.
     * 
     * Data stored as struct of arrays:
     *                                              numRegen
     *                                                 v
     *          players: [  p1  ] [  p2  ] [  p3  ] [ null ] [ null ] ...
     * tick until regen: [  4   ] [  3   ] [  0   ] [  0   ] [  0   ] ...
     * 
     * On each update, we iterate through arrays until length numRegen,
     * decrement player's regen tick. When tick reaches 0, we do player
     * health regen.
     * 
     * We double buffer these. On each update tick, we have unfilled temp
     * buffers. On each player update, we re-insert players that still need
     * regen (health < max) to the temp buffers. The temp buffers are
     * swapped in for the next tick.
     */
    private class HealthRegenTask(
        val plugin: OldRegenPlugin,
    ): BukkitRunnable() {
        override public fun run() {
            // periodically scan players, check if need to add to regen players array
            if ( plugin.checkPlayerRegenTick <= 0 ) {
                for ( player in Bukkit.getServer().getOnlinePlayers().toList() ) {
                    if ( !player.isDead() && player.getHealth() < MAX_HEALTH && player.getFoodLevel() >= plugin.minHungerToRegen ) {
                        val uuid = player.getUniqueId()
                        if ( !plugin.regenPlayerIds.contains(uuid) ) {
                            plugin.addPlayerRegen(player, 5)
                        }
                    }
                }
                plugin.checkPlayerRegenTick = plugin.checkPlayerRegenPeriod
            }
            else {
                plugin.checkPlayerRegenTick -= 1
            }
            
            // main health regen task
            
            // new number of players regen
            var newNumRegen = 0
            // new set of player ids who are currently regening
            val newRegenPlayerIds = HashSet<UUID>(16 + plugin.numRegen)
            
            for ( i in 0 until plugin.numRegen ) {
                val player = plugin.playersRegenerating[i]
                val regenTick = plugin.regenTick[i]
                if ( player !== null ) {
                    try {
                        if ( player.isOnline() && !player.isDead() && player.getHealth() < MAX_HEALTH && player.getFoodLevel() >= plugin.minHungerToRegen ) {
                            val nextRegenTick: Int = if ( regenTick <= 0 ) {
                                player.setHealth(kotlin.math.min(player.getHealth() + plugin.regenRate, MAX_HEALTH))
                                plugin.regenPeriod
                            }
                            else {
                                regenTick - 1
                            }
                            newRegenPlayerIds.add(player.getUniqueId())
                            plugin.tempBufferPlayers[newNumRegen] = player
                            plugin.tempBufferTick[newNumRegen] = nextRegenTick
                            newNumRegen += 1
                        }
                        else {
                            plugin.playersRegenerating[i] = null
                        }
                    }
                    catch ( err: Exception ) {
                        plugin.logger.severe("Failed ${err}")
                        err.printStackTrace()
                        plugin.playersRegenerating[i] = null
                    }
                }

                // remove ref, avoid memory leak
                plugin.playersRegenerating[i] = null
            }

            // double buffer update
            val temp1 = plugin.playersRegenerating
            val temp2 = plugin.regenTick
            plugin.playersRegenerating = plugin.tempBufferPlayers
            plugin.regenTick = plugin.tempBufferTick
            plugin.regenPlayerIds = newRegenPlayerIds
            plugin.numRegen = newNumRegen
            plugin.tempBufferPlayers = temp1
            plugin.tempBufferTick = temp2
        }
    }

    /**
     * Add player to regen buffer
     */
    public fun addPlayerRegen(player: Player, delay: Int) {
        this.regenPlayerIds.add(player.getUniqueId());
        this.playersRegenerating[numRegen] = player
        this.regenTick[numRegen] = delay
        this.numRegen += 1
    }

    /**
     * Load config, update settings
     * Returns number of flags loaded
     */
    public fun loadConfig() {
        // get config file
        val configFile = File(this.getDataFolder().getPath(), "config.yml")
        if ( !configFile.exists() ) {
            this.getLogger().info("No config found: generating default config.yml")
            this.saveDefaultConfig()
        }
        else {
            val config = YamlConfiguration.loadConfiguration(configFile)

            this.maxPlayers = config.getInt("maxPlayers", this.maxPlayers)
            this.regenPeriod = config.getInt("regenPeriod", this.regenPeriod)
            this.minHungerToRegen = config.getInt("minHungerToRegen", this.minHungerToRegen)
            this.checkPlayerRegenPeriod = config.getInt("checkPlayerRegenPeriod", this.checkPlayerRegenPeriod)
            this.regenDelay = config.getInt("regenDelay", this.regenDelay)
            this.regenRate = config.getDouble("regenRate", this.regenRate)

            this.getLogger().info("maxPlayers: ${this.maxPlayers}")
            this.getLogger().info("regenPeriod: ${this.regenPeriod}")
            this.getLogger().info("minHungerToRegen: ${this.minHungerToRegen}")
            this.getLogger().info("checkPlayerRegenPeriod: ${this.checkPlayerRegenPeriod}")
            this.getLogger().info("regenDelay: ${this.regenDelay}")
            this.getLogger().info("regenRate: ${this.regenRate}")
        }
    }

    public fun reload() {
        this.task?.cancel()

        this.loadConfig()

        // re-create player update buffers
        val maxPlayers = this.maxPlayers
        this.tempBufferPlayers = Array(maxPlayers, {_ -> null})
        this.tempBufferTick = IntArray(maxPlayers)

        val resizedPlayersRegenerating: Array<Player?> = Array(maxPlayers, {_ -> null})
        val resizedRegenTick = IntArray(maxPlayers)
        for ( i in 0 until this.numRegen ) {
            resizedPlayersRegenerating[i] = this.playersRegenerating[i]
            resizedRegenTick[i] = this.regenTick[i]
        }

        this.playersRegenerating = resizedPlayersRegenerating
        this.regenTick = resizedRegenTick

        val newTask = HealthRegenTask(this)
        newTask.runTaskTimer(this, 0L, 0L)
        this.task = newTask
    }

    override fun onEnable() {
        // measure load time
        val timeStart = System.currentTimeMillis()
        val logger = this.getLogger()
        
        // loads config + task
        this.reload()

        // register listener
        this.getServer().getPluginManager().registerEvents(OldRegenListener(this), this)

        // print load time
        val timeEnd = System.currentTimeMillis()
        val timeLoad = timeEnd - timeStart
        logger.info("Enabled in ${timeLoad}ms")

        // print success message
        logger.info("now this is epic")
    }

    override fun onDisable() {
        this.task?.cancel()
        logger.info("wtf i hate xeth now")
    }
}
